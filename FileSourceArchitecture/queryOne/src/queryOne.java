import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.api.java.tuple.Tuple6;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

/**
 * @authors     Giorgio Marini, Davide Nanni
 * @since       09/07/2017
 */
public class queryOne
{
    private static String   sourceRawData = "",
                            PATH_OUTPUT_DIRECTORY = "";

    private static final double MILLI_STDUNIT_CAST = 0.001;

    private static void readTestParameter(String[] args)
    {
        if (args.length != 2) {
            System.out.println("[ERROR] Invalid Input! Required <path-source> <path-results> arguments");
            System.exit(1);
        }

        sourceRawData = args[0];

        PATH_OUTPUT_DIRECTORY = args[1];
    }

    public static void main(String[] args) throws Exception
    {
        readTestParameter(args);

        StreamExecutionEnvironment env = StreamExecutionEnvironment .getExecutionEnvironment();

        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.setBufferTimeout(1);

        /**
         * Preprocessing raw data
         */
        DataStream<Tuple6<String, Integer, Long, Integer, Integer, Double>> rawData =

                env   .readTextFile(sourceRawData)
                      .setParallelism(1)
                      .flatMap( new getSidPosSpeed())
                      .assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor
                          <Tuple6<String, Integer, Long, Integer, Integer, Double>>(Time.milliseconds(100))
                            {
                                @Override
                                public long extractTimestamp(Tuple6<String, Integer, Long, Integer, Integer, Double> t)
                                {
                                    return t.f2;
                                }
                            });


        DataStream<Tuple6<String, Integer, Double, Integer, Integer, Double>> rawDataWithoutTimestamp =
                                                                                        rawData .flatMap( new deleteTimestamp());

                /**
                 *  TimeWindow: all dataset
                 */
       DataStream<Tuple6<Long, Long, String, Integer, Double, Double>> statsPlayerW1_singleLeg = rawDataWithoutTimestamp
                                                                                                        .keyBy(0,1)
                .timeWindow(Time.minutes(1))
                .apply( new calcStatPlayerSingleLeg());

        DataStream<Tuple5<Long, Long, String, Double, Double>> statsPlayerW1 = statsPlayerW1_singleLeg .keyBy(0,1,2)
                .countWindow(2)
                .reduce( new calcStatPlayer())
                .flatMap( new deleteSidInfo());

        DataStream<Tuple5<Long, Long, String, Double, Double>> statsPlayerW5 = statsPlayerW1
                                                                                        .keyBy(2)
                                                                                        .countWindow(5)
                                                                                        .apply( new calcTimeWindow());

        DataStream<Tuple5<Long, Long, String, Double, Double>> statsPlayerW30 = statsPlayerW5
                                                                                        .keyBy(2)
                                                                                        .countWindow(6)
                                                                                        .apply( new calcTimeWindow());


        statsPlayerW1   .writeAsText( PATH_OUTPUT_DIRECTORY+"/queryOne/timeWindow1.txt")
                        .setParallelism(1);

        statsPlayerW5   .writeAsText( PATH_OUTPUT_DIRECTORY+"/queryOne/timeWindow5.txt")
                        .setParallelism(1);

        statsPlayerW30  .writeAsText( PATH_OUTPUT_DIRECTORY+"/queryOne/timeWindow30.txt")
                        .setParallelism(1);

        env.execute("Query One");
    }

    /**
     * PreProcessing Raw Data
     */
    public static class getSidPosSpeed implements FlatMapFunction<  String,
            Tuple6<String, Integer, Long, Integer, Integer, Double>>
    {

        @Override
        public void flatMap(String s,
                            Collector<Tuple6<String, Integer, Long, Integer, Integer, Double>> out) throws Exception
        {
            String[] event = s.split(",");

            String playername = event[0];

            int     sid = Integer.parseInt(event[1]),
                    x = Integer.parseInt(event[3]),
                    y = Integer.parseInt(event[4]);

            long timestamp = Long.parseLong(event[2]);

            double absSpeed = Double.parseDouble(event[5]);

            out.collect( new Tuple6<String, Integer, Long, Integer, Integer, Double>
                    ( playername, sid, timestamp, x, y, absSpeed));
        }
    }


    /**
     * QueryOne
     */
    public static class deleteTimestamp implements FlatMapFunction<
                                                Tuple6<String, Integer, Long, Integer, Integer, Double>,
                                                Tuple6<String, Integer, Double, Integer, Integer, Double>>
    {
        @Override
        public void flatMap(Tuple6<String, Integer, Long, Integer, Integer, Double> t1,
                            Collector<Tuple6<String, Integer, Double, Integer, Integer, Double>> out) throws Exception
        {
            out.collect( new Tuple6<String, Integer, Double, Integer, Integer, Double>
                    (t1.f0, t1.f1, 0.0, t1.f3, t1.f4, t1.f5));
        }
    }

    public static class calcStatPlayerSingleLeg implements WindowFunction<    Tuple6< String, Integer, Double, Integer, Integer, Double>,
            Tuple6< Long, Long, String, Integer, Double, Double>,
            Tuple, TimeWindow>
    {
        @Override
        public void apply(Tuple key, TimeWindow timeWindow,
                          Iterable<Tuple6<String, Integer, Double, Integer, Integer, Double>> t,
                          Collector<Tuple6<Long, Long, String, Integer, Double, Double>> out) throws Exception
        {
            double  avgSpeed = 0.0,
                    totDistance = 0.0;

            int sid = 0,
                i = 0;

            String player = "";



            Tuple6<String, Integer, Double, Integer, Integer, Double> prevTuple =
                    new Tuple6<String, Integer, Double, Integer, Integer, Double>
                            ("", 0, 0.0, 0, 0, 0.0);

            for(Tuple6<String, Integer, Double, Integer, Integer, Double> tmp: t)
            {

                player = tmp.f0;
                sid = tmp.f1;

                avgSpeed += tmp.f5;
                totDistance += pitagora2D( prevTuple, tmp);

                prevTuple = tmp;

                ++i;
            }

            avgSpeed = avgSpeed / i;

            out.collect( new Tuple6<Long, Long, String, Integer, Double, Double>
                    (       timeWindow.getStart(), timeWindow.getEnd(),
                            player,
                            sid,
                            totDistance,
                            avgSpeed));
        }

        private double pitagora2D ( Tuple6<String, Integer, Double, Integer, Integer, Double> t1,
                                    Tuple6<String, Integer, Double, Integer, Integer, Double> t2)
        {
            return Math.sqrt(
                    (
                            Math.pow(t2.f3-t1.f3, 2)  +             // (x2 - x1)^2
                                    Math.pow(t2.f4-t1.f4, 2)        // (y2 - y1)^2
                    )
            ) * MILLI_STDUNIT_CAST;
        }
    }

    public static class calcStatPlayer implements ReduceFunction<Tuple6<Long, Long, String, Integer, Double, Double>>
    {

        @Override
        public Tuple6<Long, Long, String, Integer, Double, Double> reduce(
                Tuple6<Long, Long, String, Integer, Double, Double> t1,
                Tuple6<Long, Long, String, Integer, Double, Double> t2) throws Exception
        {
            double totDistance = (t1.f4 + t2.f4) / 2,
                    avgSpeed = (t1.f5 + t2.f5) / 2;


            return new Tuple6<Long, Long, String, Integer, Double, Double>
                    (t1.f0, t1.f1, t1.f2, t1.f3, totDistance, avgSpeed);
        }
    }


    public static class deleteSidInfo implements FlatMapFunction<Tuple6<Long, Long, String, Integer, Double, Double>,
            Tuple5<Long, Long, String, Double, Double>>
    {

        @Override
        public void flatMap(Tuple6<Long, Long, String, Integer, Double, Double> t1,
                            Collector<Tuple5<Long, Long, String, Double, Double>> out) throws Exception
        {
            out.collect( new Tuple5<Long, Long, String, Double, Double>
                    (t1.f0, t1.f1, t1.f2, t1.f4, t1.f5));
        }
    }


    public static class calcTimeWindow implements WindowFunction <Tuple5<Long, Long, String, Double, Double>,
            Tuple5< Long, Long, String, Double, Double>,
            Tuple, GlobalWindow>
    {

        @Override
        public void apply(Tuple tuple, GlobalWindow timeWindow,
                          Iterable<Tuple5<Long, Long, String, Double, Double>> t,
                          Collector<Tuple5<Long, Long, String, Double, Double>> out) throws Exception
        {
            double  avgSpeed = 0.0,
                    totDistance = 0.0;

            int i = 0;

            long minTimestamp = 0L, maxTimestamp = 0L;

            String player = "";

            for(Tuple5<Long, Long, String, Double, Double> tmp: t)
            {
                if ( i == 0 )
                    minTimestamp = tmp.f0;

                maxTimestamp = tmp.f1;

                player = tmp.f2;

                totDistance += tmp.f3;
                avgSpeed += tmp.f4;

                ++i;
            }

            avgSpeed = avgSpeed / i;

            out.collect( new Tuple5<Long, Long, String, Double, Double>
                    (       minTimestamp,
                            maxTimestamp,
                            player,
                            totDistance,
                            avgSpeed));
        }
    }
}
