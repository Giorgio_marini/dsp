import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.*;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaProducer09;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;
import org.apache.flink.util.Collector;

import java.util.Properties;

/**
 * Flink Job consumer of an Apache Kafka'a topic
 *
 * @author  Davide Nanni
 * @since 21/06/2017
 *
 */
public class FlinkJob {
    private static final long TIMESTAMP_START_GAME = 10753295594424116L;
    private static final long TIMESTAMP_STOP_GAME = 12557295594424116L;

    private static String PATH_OUTPUT_DIRECTORY = ""; //"/home/davide/FlinkResults/queryOne";

    private static final double MICRO_STDUNIT_CAST = 0.000001;
    private static final double MILLI_STDUNIT_CAST = 0.001;

    private static String   playername = "",
                            groupId = "",
                            dataset;

    private static int windowSizeDataset = 0;

    private static void readTypeDataset(String[] args)
    {
        if (args.length != 3) {
            System.out.println("[ERROR] Invalid Input! Required <type-dataset> <playername> <path-results> arguments");
            System.exit(1);
        }

        dataset = args[0];
        playername = args[1];
        groupId = "groupId-"+playername;
        PATH_OUTPUT_DIRECTORY = args[2];
    }


    private static void settingWindowDataset()
    {
        if (dataset.equals("sixth-game"))          /* 10 min */
            windowSizeDataset = 2;

        if (dataset.equals("quarter-game"))        /* 15 min */
            windowSizeDataset = 3;

        if (dataset.equals("half-game"))           /* 30 min */
            windowSizeDataset = 6;

        if (dataset.equals("full-game"))           /* 60 min */
            windowSizeDataset = 12;

    }


    public static void main(String[] args) throws Exception
    {
        readTypeDataset(args);

        settingWindowDataset();

        System.out.println("[group-id]  " + groupId);
        System.out.println("[topicname]  " + playername);

        // create execution environment
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.setBufferTimeout(1);

        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "localhost:9092");
        properties.setProperty("zookeeper.connect", "localhost:2181");
        properties.setProperty("group.id", "" + groupId);


        FlinkKafkaConsumer09<String>    kafkaSource = new FlinkKafkaConsumer09<>(playername, new SimpleStringSchema(), properties);

        FlinkKafkaProducer09<String>    kafkaProducerW1 = new FlinkKafkaProducer09<>( "localhost:9092",
                                                                                    "w1",
                                                                                     new SimpleStringSchema()),

                                        kafkaProducerW5 = new FlinkKafkaProducer09<>( "localhost:9092",
                                                                                            "w5",
                                                                                        new SimpleStringSchema()),

                                        kafkaProducerD = new FlinkKafkaProducer09<>( "localhost:9092",
                                                                                            "allDataset",
                                                                                        new SimpleStringSchema());


        kafkaSource.assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<String>(Time.milliseconds(100)) {
            @Override
            public long extractTimestamp(String s)
            {
                String[] event = s.split(",");

                return Long.parseLong(event[2]);
            }
        });

        /**
         *  Query One
         */
                    /**
                     *  Time Window:    1 min
                     *
                     */
        DataStream<Tuple6<Long, Long, String, Integer, Double, Double>> statsPlayerW1_singleLeg = env .addSource(kafkaSource)
                                                                                                      .flatMap( new outSidStream())
                                                                                                      .keyBy(1)
                                                                                                      .timeWindow(Time.minutes(1))
                                                                                                      .apply( new newnewStatPlayerSingleLeg());

        DataStream<Tuple5<Long, Long, String, Double, Double>> statsPlayerW1 = statsPlayerW1_singleLeg .keyBy(0,1)
                                                                                                       .countWindow(2)
                                                                                                       .reduce( new newStatPlayer())
                                                                                                       .flatMap( new deleteSidInfo());

                    /**
                     *  Time Window:    5 min
                     *
                     */

        DataStream<Tuple6<Long, Long, String, Integer, Double, Double>> statsPlayerW5_singleLeg = env .addSource(kafkaSource)
                                                                                                      .flatMap( new outSidStream())
                                                                                                      .keyBy(1)
                                                                                                      .timeWindow(Time.minutes(5))
                                                                                                      .apply( new newnewStatPlayerSingleLeg());

        DataStream<Tuple5<Long, Long, String, Double, Double>> statsPlayerW5 = statsPlayerW5_singleLeg .keyBy(0,1)
                                                                                                       .countWindow(2)
                                                                                                       .reduce( new newStatPlayer())
                                                                                                       .flatMap( new deleteSidInfo());


                    /**
                     *  Time Window:    All Dataset
                     *
                     */
        DataStream<Tuple5<Long, Long, String, Double, Double>> statsPlayerD = statsPlayerW5 .keyBy(2)
                                                                                            .countWindow( windowSizeDataset )
                                                                                            .apply( new avgStatPlayerDataset());

                    /**
                     *  Prints the results
                     *
                     */

        //statsPlayerW1_singleLeg.writeAsText( PATH_OUTPUT_DIRECTORY+"/w1/singleLeg/"+topicname+".txt");
        statsPlayerW1.writeAsText( PATH_OUTPUT_DIRECTORY+"/queryOne/w1/"+playername+".txt");
        //statsPlayerW5_singleLeg.writeAsText( PATH_OUTPUT_DIRECTORY+"/w5/singleLeg/"+topicname+".txt");
        statsPlayerW5.writeAsText( PATH_OUTPUT_DIRECTORY+"/queryOne/w5/"+playername+".txt");
        statsPlayerD.writeAsText( PATH_OUTPUT_DIRECTORY+"/queryOne/allDataset/"+playername+".txt");


                    /**
                     *  Sends the results to three Kafka Topic for QueryTwo processing
                     *
                     */
        DataStream<String>  outStatsPlayerW1 = statsPlayerW1.flatMap( new castTupleToString()),

                            outStatsPlayerW5 = statsPlayerW5.flatMap( new castTupleToString()),

                            outStatsPlayerD = statsPlayerD.flatMap( new castTupleToString());


        outStatsPlayerW1.addSink(kafkaProducerW1);
        outStatsPlayerW5.addSink(kafkaProducerW5);
        outStatsPlayerD.addSink(kafkaProducerD);


        /**
         *  Query Three
         */
                    /**
                     *  Time Window:    10 min
                     *
                     */
        DataStream<Tuple4<Long, String, Integer, Double[]>> statsPlayerW10_singleLeg = env  .addSource(kafkaSource)
                                                                                            .flatMap(new outPosXY())
                                                                                            .keyBy(1)
                                                                                            .timeWindow(Time.minutes(10))
                                                                                            .apply(new posStatPlayerSingleLeg());

        DataStream<Tuple3<Long, String, Double[]>> statsPlayerW10 = statsPlayerW10_singleLeg.keyBy(0)
                                                                                            .countWindow(2)
                                                                                            .reduce(new posStatPlayer())
                                                                                            .flatMap(new deleteSidInfoPos());

        DataStream<Tuple2<Long, String>> outputStatsPlayerW10 = statsPlayerW10.flatMap(new outputFormatStringPos());

                    /**
                     *  Time Window:    All Dataset
                     *
                     */
        DataStream<Tuple3<Long, String, Double[]>> statsPlayerPosD = statsPlayerW10 .keyBy(1)
                                                                                    .countWindow(windowSizeDataset/2)
                                                                                    .apply(new calcStatsPlayerDatasetPos());

        DataStream<Tuple2<Long, String>> outputStatsPlayerPosD = statsPlayerW10.flatMap(new outputFormatStringPos());


        statsPlayerW10_singleLeg.writeAsText(PATH_OUTPUT_DIRECTORY + "/queryThree/w10/singleLeg/" + playername + ".txt");
        outputStatsPlayerW10.writeAsText(PATH_OUTPUT_DIRECTORY + "/queryThree/w10/" + playername + ".txt");

        outputStatsPlayerPosD.writeAsText(PATH_OUTPUT_DIRECTORY + "/queryThree/allDataset/output/" + playername + ".txt");

       env.execute("Query One - Three : "+playername);
    }


    public static class outSidStream implements FlatMapFunction<String, Tuple7< String, Integer, Double, Integer, Integer, Integer, Double>>
    {

        @Override
        public void flatMap(String s, Collector<Tuple7<String, Integer, Double, Integer, Integer, Integer, Double>> out) throws Exception
        {
            String[] event = s.split(",");

            String player = event[0];

            int     sid = Integer.parseInt(event[1]),
                    x = Integer.parseInt(event[3]),
                    y = Integer.parseInt(event[4]),
                    z = Integer.parseInt(event[5]);
            double magnitudeSpeed = Double.parseDouble(event[6]) * MICRO_STDUNIT_CAST;

            out.collect( new Tuple7<String, Integer, Double, Integer, Integer, Integer, Double>
                            (player, sid, 0.0, x, y, z, magnitudeSpeed));
        }
    }



    public static class newnewStatPlayerSingleLeg implements WindowFunction<    Tuple7< String, Integer, Double, Integer, Integer, Integer, Double>,
                                                                                Tuple6< Long, Long, String, Integer, Double, Double>,
                                                                                Tuple, TimeWindow>
    {
        private int i = 0;

        @Override
        public void apply(Tuple key, TimeWindow timeWindow,
                          Iterable<Tuple7<String, Integer, Double, Integer, Integer, Integer, Double>> t,
                          Collector<Tuple6<Long, Long, String, Integer, Double, Double>> out) throws Exception
        {
            double  avgSpeed = 0.0,
                    totDistance = 0.0;

            int sid = 0;

            String player = "";



            Tuple7<String, Integer, Double, Integer, Integer, Integer, Double> prevTuple =
                        new Tuple7<String, Integer, Double, Integer, Integer, Integer, Double>
                                ("", 0, 0.0, 0, 0, 0, 0.0);

            for(Tuple7<String, Integer, Double, Integer, Integer, Integer, Double> tmp: t)
            {

                player = tmp.f0;
                sid = tmp.f1;

                avgSpeed += tmp.f6;
                totDistance += pitagora2D( prevTuple, tmp);

                prevTuple = tmp;

                ++i;
            }

            avgSpeed = avgSpeed / i;

            out.collect( new Tuple6<Long, Long, String, Integer, Double, Double>
                    (       timeWindow.getStart(), timeWindow.getEnd(),
                            player,
                            sid,
                            totDistance,
                            avgSpeed));
        }

        private double pitagora2D ( Tuple7<String, Integer, Double, Integer, Integer, Integer, Double> t1,
                                    Tuple7<String, Integer, Double, Integer, Integer, Integer, Double> t2)
        {
            return Math.sqrt(
                    (
                            Math.pow(t2.f3-t1.f3, 2)  +             // (x2 - x1)^2
                            Math.pow(t2.f4-t1.f4, 2)                // (y2 - y1)^2
                    )
            ) * MILLI_STDUNIT_CAST;
        }
    }






    public static class newStatPlayer implements ReduceFunction <Tuple6<Long, Long, String, Integer, Double, Double>>
    {

        @Override
        public Tuple6<Long, Long, String, Integer, Double, Double> reduce(
                            Tuple6<Long, Long, String, Integer, Double, Double> t1,
                            Tuple6<Long, Long, String, Integer, Double, Double> t2) throws Exception
        {
            double totDistance = (t1.f4 + t2.f4) / 2,
                    avgSpeed = (t1.f5 + t2.f5) / 2;


            return new Tuple6<Long, Long, String, Integer, Double, Double>
                    (t1.f0, t1.f1, t1.f2, t1.f3, totDistance, avgSpeed);
        }
    }


    public static class deleteSidInfo implements FlatMapFunction<Tuple6<Long, Long, String, Integer, Double, Double>,
                                                                 Tuple5<Long, Long, String, Double, Double>>
    {

        @Override
        public void flatMap(Tuple6<Long, Long, String, Integer, Double, Double> t1,
                            Collector<Tuple5<Long, Long, String, Double, Double>> out) throws Exception
        {
            out.collect( new Tuple5<Long, Long, String, Double, Double>
                    (t1.f0, t1.f1, t1.f2, t1.f4, t1.f5));
        }
    }


    public static class avgStatPlayerDataset implements WindowFunction <Tuple5<Long, Long, String, Double, Double>,
                                                                        Tuple5< Long, Long, String, Double, Double>,
                                                                        Tuple, GlobalWindow>
    {

        private int i = 0;

        @Override
        public void apply(Tuple tuple, GlobalWindow globalWindow,
                          Iterable<Tuple5<Long, Long, String, Double, Double>> t,
                          Collector<Tuple5<Long, Long, String, Double, Double>> out) throws Exception
        {
            double  avgSpeed = 0.0,
                    totDistance = 0.0;

            long timestamp_endWindow = 0L;

            String player = "";

            for(Tuple5<Long, Long, String, Double, Double> tmp: t)
            {
                timestamp_endWindow = tmp.f1;

                player = tmp.f2;

                totDistance += tmp.f3;
                avgSpeed += tmp.f4;

                ++i;
            }

            avgSpeed = avgSpeed / i;

            out.collect( new Tuple5<Long, Long, String, Double, Double>
                    (       0L,
                            timestamp_endWindow,
                            player,
                            totDistance,
                            avgSpeed));
        }
    }


    public static class castTupleToString implements FlatMapFunction<Tuple5<Long, Long, String, Double, Double>, String>
    {

        @Override
        public void flatMap(Tuple5<Long, Long, String, Double, Double> t, Collector<String> out) throws Exception
        {
            out.collect(     ""+t.f0+","+t.f1+","+t.f2+","+t.f3+","+t.f4);
        }
    }


    /**
     * QueryThree Function
     */
    public static class outPosXY implements FlatMapFunction<String, Tuple4<String, Integer, Integer, Integer>> {

        @Override
        public void flatMap(String s, Collector<Tuple4<String, Integer, Integer, Integer>> out) throws Exception {
            String[] event = s.split(",");

            String player = event[0];

            int sid = Integer.parseInt(event[1]), x = Integer.parseInt(event[3]), y = Integer.parseInt(event[4]);

            out.collect(new Tuple4<String, Integer, Integer, Integer>(player, sid, x, y));
        }
    }


    public static class posStatPlayerSingleLeg implements WindowFunction<   Tuple4<String, Integer, Integer, Integer>,
                                                                            Tuple4<Long, String, Integer, Double[]>,
                                                                            Tuple, TimeWindow> {
        private int limSupY = 67930, limSupX = 52489;

        private Double[] fieldSingleLeg = new Double[104];

        private void initField() {
            int j = 0;

            for (; j < 104; ++j)
                fieldSingleLeg[j] = 0.0;
        }

        private int adaptYPos(int oldY) {
            int limSup = 33965;

            if (oldY >= 0) return Math.abs(oldY - limSup);
            else return Math.abs(oldY) + limSup;
        }

        private void calcPercTime(int i) {
            int j = 0;

            for (; j < 104; ++j)
                fieldSingleLeg[j] = fieldSingleLeg[j] / i * 100;
        }

        @Override
        public void apply(Tuple key, TimeWindow timeWindow, Iterable<Tuple4<String, Integer, Integer, Integer>> t, Collector<Tuple4<Long, String, Integer, Double[]>> out) throws Exception {
            initField();

            String player = "";
            int sid = 0, i = 0;


            for (Tuple4<String, Integer, Integer, Integer> tmp : t) {
                player = tmp.f0;
                sid = tmp.f1;

                int y = adaptYPos(tmp.f3), posField = Math.round(y * 8 / limSupY) + Math.round(tmp.f2 * 13 / limSupX);

                fieldSingleLeg[posField]++;
                ++i;
            }

            calcPercTime(i);

            out.collect(new Tuple4<Long, String, Integer, Double[]>(timeWindow.getEnd(), player, sid, fieldSingleLeg));
        }
    }

    public static class posStatPlayer implements ReduceFunction<Tuple4<Long, String, Integer, Double[]>> {
        String result = "";

        private Double[] field = new Double[104];

        private void initField() {
            int j = 0;

            for (; j < 104; ++j)
                field[j] = 0.0;
        }

        private void calcAvgStats(Tuple4<Long, String, Integer, Double[]> t1, Tuple4<Long, String, Integer, Double[]> t2) {
            int j = 0;

            for (; j < 104; ++j) {
                field[j] = (t1.f3[j] + t2.f3[j]) / 2;
                //          result += "cell"+j+","+field[j]+",";
            }
        }


        @Override
        public Tuple4<Long, String, Integer, Double[]> reduce(Tuple4<Long, String, Integer, Double[]> t1, Tuple4<Long, String, Integer, Double[]> t2) throws Exception {
            //    result = t1.f1+",";
            initField();

            calcAvgStats(t1, t2);

            return (new Tuple4<Long, String, Integer, Double[]>(t1.f0, t1.f1, t1.f2, field));

        }
    }


    public static class deleteSidInfoPos implements FlatMapFunction<Tuple4<Long, String, Integer, Double[]>,
                                                                    Tuple3<Long, String, Double[]>> {
        public void flatMap(Tuple4<Long, String, Integer, Double[]> t1,
                            Collector<Tuple3<Long, String, Double[]>> out) {
            out.collect(new Tuple3<Long, String, Double[]>(t1.f0, t1.f1, t1.f3));
        }
    }

    public static class outputFormatStringPos implements FlatMapFunction<Tuple3<Long, String, Double[]>,
                                                                            Tuple2<Long, String>> {
        String result = "";


        private void writeResult(Tuple3<Long, String, Double[]> t) {
            int i = 0;

            for (double avg : t.f2) {
                result += "cell" + i + "," + avg + ",";
                ++i;
            }
        }

        @Override
        public void flatMap(Tuple3<Long, String, Double[]> t1, Collector<Tuple2<Long, String>> out) throws Exception {
            result = t1.f1+",";

            writeResult(t1);

            out.collect(new Tuple2<Long, String>(t1.f0, result));
        }
    }

    public static class calcStatsPlayerDatasetPos implements WindowFunction<Tuple3<Long, String, Double[]>, Tuple3<Long, String, Double[]>, Tuple, GlobalWindow> {
        private Double[] field = new Double[104];

        private void initField() {
            int j = 0;

            for (; j < 104; ++j)
                field[j] = 0.0;
        }

        private void sumValuePerc(Tuple3<Long, String, Double[]> t) {
            int j = 0;

            for (; j < 104; ++j)
                field[j] += t.f2[j];
        }

        private void calcAvgStat(int i) {
            int j = 0;

            for (; j < 104; ++j)
                field[j] = field[j] / i;
        }

        @Override
        public void apply(Tuple tuple, GlobalWindow globalWindow, Iterable<Tuple3<Long, String, Double[]>> t1, Collector<Tuple3<Long, String, Double[]>> out) throws Exception {
            initField();

            int i = 0;
            String player = "";
            Long timestamp = 0L;

            for (Tuple3<Long, String, Double[]> tmp : t1) {
                timestamp = tmp.f0;
                player = tmp.f1;

                sumValuePerc(tmp);

                ++i;
            }

            calcAvgStat(i);

            out.collect(new Tuple3<Long, String, Double[]>(timestamp, player, field));
        }
    }

}
