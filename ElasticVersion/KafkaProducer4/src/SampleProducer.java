import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Properties;

/**
 * Apache Kafka Producer
 *
 *      It reads the events from "full-game" file and sends they to the relative player Apache Kafka's topic
 *
 * @author  Davide Nanni
 * @since 20/06/2017
 *
 */
public class SampleProducer {

    private static final double MILLI_STDUNIT_CAST = 0.000000001;
    private static final long TIMESTAMP_START_GAME = 10753295594424116L;
    private static final long TIMESTAMP_STOP_GAME = 12557295594424116L;

    private static long startNextEvent = TIMESTAMP_START_GAME;

    private static Producer<String, String> producer;

    private static int readPathDataset(String[] args)
    {
        int limEntries = 0;


        if ( (args.length != 1) && (args.length != 2))
        {
            System.out.println("[ERROR] Invalid Input! Required one <path_dataset> argument or <path_dataset> <num_lines>.");
            System.exit(1);
        }

        if ( args.length == 1 )
        {
            limEntries = -1;
        }
        else
        {
            if ( args.length == 2 )
            {
                limEntries = Integer.parseInt(args[1]);

                if ( limEntries < 5000000 )
                {
                    System.out.println("[ERROR] The <num_lines> integer value must be >= 5000000");
                    System.exit(1);
                }
            }
        }

        return limEntries;
    }


    public static void main(String[] args) throws Exception
    {
        int i = 0, lim = 0;

        boolean allEntries = false;


        initKafkaProducer();

        lim = readPathDataset(args);


        if ( lim == -1 )
        {
            allEntries = true;
        }

        BufferedReader buff = new BufferedReader(new FileReader(args[0]));

        String event = "";

        while( ( allEntries || ( i < lim )) && ((event = buff.readLine()) != null))
        {
                            /**
                             *  Send the sensor event
                             */
            sendPlayerEvent( event );

            if ( !allEntries )
                ++i;
        }

        producer.close();
    }


    private static void initKafkaProducer()
    {
        // create instance for properties to access producer configs
        Properties props = new Properties();

        //Assign localhost id
        props.put("bootstrap.servers", "localhost:9092");

        //Set acknowledgements for producer requests.
        props.put("acks", "all");

        //If the request fails, the producer can automatically retry,
        props.put("retries", 0);

        //Specify buffer size in config
        props.put("batch.size", 16384);

        //Reduce the no of requests less than 0
        props.put("linger.ms", 1);

        //The buffer.memory controls the total amount of memory available to the producer for buffering.
        props.put("buffer.memory", 33554432);

        props.put("key.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");

        props.put("value.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");

        producer = new KafkaProducer<String, String>(props);
    }


    private static void sendPlayerEvent(String event) throws InterruptedException {

        String  player = "",
                team = "";

        boolean eventSid = false;

        int sid = Integer.parseInt(event.split(",")[0]);

        switch( sid )
        {

            /**
             *  TEAM A
             *
             */

            case 13:
            case 14:
            //case 97:                      Left Arm Sensor
            //case 98:                      Right Arm Sensor
                        player = "Nick Gertje";
                        eventSid = true;
                        break;

            case 47:
            case 16:
                        player = "Dennis Dotterweicj";
                        eventSid = true;
                        break;

            case 49:
            case 88:
                        player = "Niklas Waelzlein";
                        eventSid = true;
                        break;

            case 19:
            case 52:
                        player = "Wili Sommer";
                        eventSid = true;
                        break;

            case 53:
            case 54:
                        player = "Philipp Harlass";
                        eventSid = true;
                        break;

            case 23:
            case 24:
                        player = "Roman Hartleb";
                        eventSid = true;
                        break;

            case 57:
            case 58:
                        player = "Erik Engelhardt";
                        eventSid = true;
                        break;

            case 59:
            case 28:
                        player = "Sandro Schneider";
                        eventSid = true;
                        break;

            /**
             *  TEAM B
             *
             */

            case 61:
            case 62:
            //case 99:                      Left Arm Sensor
            //case 100:                     Right Arm Sensor
                        player = "Leon Krapf";
                        eventSid = true;
                        break;

            case 63:
            case 64:
                        player = "Kevin Baer";
                        eventSid = true;
                        break;

            case 65:
            case 66:
                        player = "Luca Ziegler";
                        eventSid = true;
                        break;

            case 67:
            case 68:
                        player = "Ben Mueller";
                        eventSid = true;
                        break;

            case 69:
            case 38:
                        player = "Vale Reistetter";
                        eventSid = true;
                        break;

            case 71:
            case 40:
                        player = "Christopher Lee";
                        eventSid = true;
                        break;

            case 73:
            case 74:
                        player = "Leon Heinze";
                        eventSid = true;
                        break;

            case 75:
            case 44:
                        player = "Leo Langhans";
                        eventSid = true;
                        break;
        }

        if ( eventSid )
        {
            event = player+","+event;

            String topicname = player.replaceAll("\\s","");

            sendMessage( topicname, event);
        }
    }

    private static void sendMessage(String topicName, String event) throws InterruptedException
    {
        long timeStart = getWaitTime(event);

        if ( timeStart != -1 ) {

            String newEvent = getEventMilliSec(event);

            Thread.sleep(timeStart);

            System.out.println("[EVENT] " + newEvent);

            producer.send(new ProducerRecord<String, String>(topicName, topicName, newEvent));
        }
    }


    private static long getWaitTime(String event)
    {
        String[] line = event.split(",");

        long absTimestampEvent = Long.parseLong(line[2]);
        long relativeStartNextEvent = 0;

        relativeStartNextEvent =  absTimestampEvent - startNextEvent;

        if (relativeStartNextEvent < 0)
        {
            startNextEvent = TIMESTAMP_START_GAME;
            return -1;
        }

        startNextEvent = absTimestampEvent;

        return Math.round(relativeStartNextEvent * MILLI_STDUNIT_CAST);
    }

    private static String getEventMilliSec(String event)
    {
        String[] line = event.split(",");

        long    absTimestampEvent = Long.parseLong(line[2]),
                relTimestampEvent = absTimestampEvent - TIMESTAMP_START_GAME,
                relTimestampEventMilli = Math.round(relTimestampEvent*MILLI_STDUNIT_CAST);

        String newEvent = line[0]+","+line[1]+","+relTimestampEventMilli+",";

        int i = 3;
        while( i < line.length)
        {
            newEvent += line[i];
            ++i;

            if ( i < line.length )
                newEvent += ",";
        }

        return newEvent;
    }
}
