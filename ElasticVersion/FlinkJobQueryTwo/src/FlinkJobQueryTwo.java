import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.*;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;
import org.apache.flink.util.Collector;

import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

/**
 * Flink Job consumer of an Apache Kafka'a topic
 *
 * @author  Davide Nanni
 * @since 21/06/2017
 *
 */
public class FlinkJobQueryTwo {

    private static String   PATH_OUTPUT_DIRECTORY = "", //"/home/davide/FlinkResults/queryTwo";
                            groupId = "";

    private static int numPlayers = 0;


    private static int getNumPlayers(String[] args)
    {
        int num_players = 0;

        num_players = Integer.parseInt(args[0]);

        if ( ( num_players < 2 ) || ( num_players > 16 ) )
        {
            System.out.println("[ERROR] Invalid Input! The <num_player> value must be between 2 and 16");
            System.exit(1);
        }

        return num_players;
    }


    private static void readTypeDataset(String[] args)
    {
        if (args.length != 2) {
            System.out.println("[ERROR] Invalid Input! Required <num_player> <path-results> arguments");
            System.exit(1);
        }

        numPlayers = getNumPlayers(args);
        groupId = "groupId-QueryTwo";

        PATH_OUTPUT_DIRECTORY = args[1];
    }



    public static void main(String[] args) throws Exception
    {
        readTypeDataset(args);

        // create execution environment
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.setBufferTimeout(1);

        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "localhost:9092");
        properties.setProperty("zookeeper.connect", "localhost:2181");
        properties.setProperty("group.id", "queryTwo");


        FlinkKafkaConsumer09<String>    kafkaSourceW1 = new FlinkKafkaConsumer09<>("w1", new SimpleStringSchema(), properties),
                                        kafkaSourceW5 = new FlinkKafkaConsumer09<>("w5", new SimpleStringSchema(), properties),
                                        kafkaSourceD = new FlinkKafkaConsumer09<>( "allDataset", new SimpleStringSchema(), properties);


        DataStream<Tuple3<Long, Long, String>> statsPlayerW1 = env  .addSource(kafkaSourceW1)
                                                                    .flatMap( new outSidStream())
                                                                    .keyBy(0,1)
                                                                    .countWindow(numPlayers)
                                                                    .apply( new calcRating());


        DataStream<Tuple3<Long, Long, String>> statsPlayerW5 = env  .addSource(kafkaSourceW5)
                                                                    .flatMap( new outSidStream())
                                                                    .keyBy(0,1)
                                                                    .countWindow(numPlayers)
                                                                    .apply( new calcRating());

        DataStream<Tuple3<Long, Long, String>> statsPlayerD = env  .addSource(kafkaSourceD)
                                                                    .flatMap( new outSidStream())
                                                                    .keyBy(0,1)
                                                                    .countWindow(numPlayers)
                                                                    .apply( new calcRating());


        statsPlayerW1.writeAsText( PATH_OUTPUT_DIRECTORY+"/w1/ranking.txt");
        statsPlayerW5.writeAsText( PATH_OUTPUT_DIRECTORY+"/w5/ranking.txt");
        statsPlayerD.writeAsText( PATH_OUTPUT_DIRECTORY+"/allDataset/ranking.txt");

       env.execute("Query Two");
    }



    public static class outSidStream implements FlatMapFunction<String, Tuple4<Long, Long, String, Double>>
    {

        @Override
        public void flatMap(String s, Collector<Tuple4<Long, Long, String, Double>> out) throws Exception
        {
            String[] event = s.split(",");

            long    timestampStart = Long.parseLong(event[0]),
                    timestampStop = Long.parseLong(event[1]);

            String player = event[2];

            double  avgSpeed = Double.parseDouble(event[4]);

            out.collect( new Tuple4<Long, Long, String, Double>
                            ( timestampStart, timestampStop, player, avgSpeed));
        }
    }

    static class RatingPlayer implements Comparable<RatingPlayer>
    {
        private String player = "";

        private double avgSpeed = 0.0;

        public RatingPlayer(String player,
                            double avgSpeed)
        {
            this.player = player;
            this.avgSpeed = avgSpeed;
        }

        public String getPlayer()
        {
            return player;
        }

        public double getAvgSpeed()
        {
            return avgSpeed;
        }

        @Override
        public int compareTo(RatingPlayer o)
        {
            if ( this.avgSpeed < o.avgSpeed )
                return 1;
            else
            {
                if ( this.avgSpeed == o.avgSpeed )
                    return 0;
                else
                    return -1;
            }
        }
    }

    private static class calcRating implements WindowFunction<  Tuple4<Long, Long, String, Double>,
                                                                Tuple3<Long, Long, String>,
                                                                Tuple, GlobalWindow>
    {
        @Override
        public void apply(Tuple tuple, GlobalWindow globalWindow,
                          Iterable<Tuple4<Long, Long, String, Double>> t1,
                          Collector<Tuple3<Long, Long, String>> t2) throws Exception
        {
            Set<RatingPlayer> p = new TreeSet<RatingPlayer>();

            long    timestamp_start = 0L,
                    timestamp_stop = 0L;

            for( Tuple4<Long, Long, String, Double> tmp: t1)
            {
                p.add( new RatingPlayer( tmp.f2, tmp.f3 ));

                timestamp_start = tmp.f0;
                timestamp_stop = tmp.f1;
            }

            Iterator<RatingPlayer> i = p.iterator();

            String finalRating =    "";

            while( i.hasNext())
            {
                RatingPlayer tmp = i.next();

                finalRating +=  tmp.getPlayer()+","+
                                tmp.getAvgSpeed()+",";
            }

            t2.collect( new Tuple3<Long, Long, String>
                            ( timestamp_start, timestamp_stop, finalRating));
        }
    }
}
