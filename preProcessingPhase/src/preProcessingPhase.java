/**
 * Created by root on 10/07/17.
 */
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple6;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.util.Collector;

/**
 * Created by root on 10/07/17.
 */
public class preProcessingPhase
{
    private static String   sourceRawData = "",
            PATH_OUTPUT_DIRECTORY = "";

    private static final long TIMESTAMP_START_FIRST_HALF = 10753295594424116L ;
    private static final long TIMESTAMP_STOP_FIRST_HALF = (long)(TIMESTAMP_START_FIRST_HALF+(31*60*Math.pow(10,12)));
    private static final long TIMESTAMP_START_SECOND_HALF = 13086639146403495L;
    private static final long TIMESTAMP_STOP_SECOND_HALF = (long)(TIMESTAMP_START_SECOND_HALF+(31*60*Math.pow(10,12)));

    private static void readTestParameter(String[] args)
    {
        if (args.length != 2) {
            System.out.println("[ERROR] Invalid Input! Required <path-source> <path-results> arguments");
            System.exit(1);
        }

        sourceRawData = args[0];

        PATH_OUTPUT_DIRECTORY = args[1];
    }

    public static void main(String[] args) throws Exception {
        readTestParameter(args);

        StreamExecutionEnvironment env = StreamExecutionEnvironment .getExecutionEnvironment();

        DataStream<Tuple6<String, Integer, Long, Integer, Integer, Double>> rawData = env   .readTextFile(sourceRawData)
                .flatMap( new getInfo())
                .filter( new filterSid())
                .filter( new filterTimestamp())
                .flatMap( new castSpeedTimestampInfo());

        DataStream<String> outputRawData = rawData.flatMap( new castToString());

        outputRawData.writeAsText(PATH_OUTPUT_DIRECTORY+"/half-game");

        env.execute("PreProcessingPhase");
    }

    public static class getInfo implements FlatMapFunction<String,
            Tuple6<String, Integer, Long, Integer, Integer, Double>>
    {

        @Override
        public void flatMap(String s,
                            Collector<Tuple6<String, Integer, Long, Integer, Integer, Double>> out) throws Exception
        {
            String[] event = s.split(",");

            int sid = Integer.parseInt(event[0]);

            String playername = addPlayer(sid);

            long timestamp = Long.parseLong(event[1]);

            int     x = Integer.parseInt(event[2]),
                    y = Integer.parseInt(event[3]);

            double v = Double.parseDouble(event[5]);

            out.collect(new Tuple6<String, Integer, Long, Integer, Integer, Double>(
                    playername,
                    sid,
                    timestamp,
                    x,
                    y,
                    v));
        }


        private static String addPlayer(int sid) {
            String player = "";
            /**
             *  Team A
             */

            if ((sid == 13) || (sid == 14)) player = "Nick Gertje";

            if ((sid == 47) || (sid == 16)) player = "Dennis Dotterweicj";


            if ((sid == 49) || (sid == 88)) player = "Niklas Waelzlein";

            if ((sid == 19) || (sid == 52)) player = "Wili Sommer";

            if ((sid == 53) || (sid == 54)) player = "Philipp Harlass";

            if ((sid == 23) || (sid == 24)) player = "Roman Hartleb";

            if ((sid == 57) || (sid == 58)) player = "Erik Engelhardt";

            if ((sid == 59) || (sid == 28)) player = "Sandro Schneider";

            /**
             *  Team B
             */

            if ((sid == 61) || (sid == 62)) player = "Leon Krapf";

            if ((sid == 63) || (sid == 64)) player = "Kevin Baer";

            if ((sid == 65) || (sid == 66)) player = "Luca Ziegler";

            if ((sid == 67) || (sid == 68)) player = "Ben Mueller";

            if ((sid == 69) || (sid == 38)) player = "Vale Reistetter";

            if ((sid == 71) || (sid == 40)) player = "Christopher Lee";

            if ((sid == 73) || (sid == 74)) player = "Leon Heinze";

            if ((sid == 75) || (sid == 44)) player = "Leo Langhans";

            return player;
        }
    }

    public static class filterSid implements FilterFunction<Tuple6<String, Integer, Long, Integer, Integer, Double>>
    {
        public boolean filter( Tuple6<String, Integer, Long, Integer, Integer, Double> t)
        {
            int sid = t.f1;

            if (    ( ( sid == 13 ) || (sid == 14)  ) ||
                    ( ( sid == 47 ) || (sid == 16)  ) ||
                    ( ( sid == 49 ) || (sid == 88)  ) ||
                    ( ( sid == 19 ) || (sid == 52)  ) ||
                    ( ( sid == 53 ) || (sid == 54)  ) ||
                    ( ( sid == 23 ) || (sid == 24)  ) ||
                    ( ( sid == 57 ) || (sid == 58)  ) ||
                    ( ( sid == 59 ) || (sid == 28)  ) ||

                    ( ( sid == 61 ) || (sid == 62)  ) ||
                    ( ( sid == 63 ) || (sid == 64)  ) ||
                    ( ( sid == 65 ) || (sid == 66)  ) ||
                    ( ( sid == 67 ) || (sid == 68)  ) ||
                    ( ( sid == 69 ) || (sid == 38)  ) ||
                    ( ( sid == 71 ) || (sid == 40)  ) ||
                    ( ( sid == 73 ) || (sid == 74)  ) ||
                    ( ( sid == 75 ) || (sid == 44)  ))
                return true;
            else
                return false;
        }
    }



    public static class filterTimestamp implements FilterFunction<Tuple6<String, Integer, Long, Integer, Integer, Double>>
    {
        public boolean filter (Tuple6<String, Integer, Long, Integer, Integer, Double> t)
        {
            long timestamp = t.f2;

            if (    ((timestamp >= TIMESTAMP_START_FIRST_HALF) && ( timestamp <= TIMESTAMP_STOP_FIRST_HALF)) ||
                    ((timestamp >= TIMESTAMP_START_SECOND_HALF) && ( timestamp <= TIMESTAMP_STOP_SECOND_HALF)))
                return true;
            else
                return false;
        }
    }

    public static class castSpeedTimestampInfo implements FlatMapFunction<   Tuple6<String, Integer, Long, Integer, Integer, Double>,
            Tuple6<String, Integer, Long, Integer, Integer, Double>> {

        private static final double CAST_FROM_PICO_TO_MILLI = 0.000000001;
        private static final double CAST_FROM_MICRO_TO_MILLI = 0.000001;

        @Override
        public void flatMap(Tuple6<String, Integer, Long, Integer, Integer, Double> t1, Collector<Tuple6<String, Integer, Long, Integer, Integer, Double>> out) throws Exception {
            long timestampMilliSeconds = castMilliTimestamp(t1.f2);

            double milliAvgSpeed = castMicroSpeed(t1.f5);


            out.collect(new Tuple6<String, Integer, Long, Integer, Integer, Double>(t1.f0, t1.f1, timestampMilliSeconds, t1.f3, t1.f4, milliAvgSpeed));
        }

        private static long castMilliTimestamp(long timestamp) {
            long startTimeGame = 0L;

            if (    (timestamp >= TIMESTAMP_START_FIRST_HALF) && ( timestamp <= TIMESTAMP_STOP_FIRST_HALF))
                startTimeGame = TIMESTAMP_START_FIRST_HALF;

            if ((timestamp >= TIMESTAMP_START_SECOND_HALF) && ( timestamp <= TIMESTAMP_STOP_SECOND_HALF))
                startTimeGame = TIMESTAMP_START_SECOND_HALF;

            return Math.round((timestamp - startTimeGame) * CAST_FROM_PICO_TO_MILLI);
        }

        private static double castMicroSpeed(double microSpeed) {
            return microSpeed * CAST_FROM_MICRO_TO_MILLI;
        }
    }


    private static class castToString implements FlatMapFunction< Tuple6<String, Integer, Long, Integer, Integer, Double>, String>
    {
        @Override
        public void flatMap(Tuple6<String, Integer, Long, Integer, Integer, Double> t1, Collector<String> out)
        {
            out.collect( t1.f0+","+t1.f1+","+t1.f2+","+t1.f3+","+t1.f4+","+t1.f5);
        }
    }
}

