import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.java.tuple.*;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;
import org.apache.flink.util.Collector;

import java.util.Properties;

/**
 * @authors Giorgio Marini, Davide Nanni
 * @since   11/07/2017
 *
 */
public class queryThree
{
    private static String   PATH_OUTPUT_DIRECTORY = "";

    private static String KafkaTopic = "playerEvents";

    private static void readTestParameter(String[] args)
    {
        if (args.length != 1) {
            System.out.println("[ERROR] Invalid Input! Required <path-results> arguments");
            System.exit(1);
        }

        PATH_OUTPUT_DIRECTORY = args[0];
    }

    public static void main(String[] args) throws Exception
    {
        readTestParameter(args);

        StreamExecutionEnvironment env = StreamExecutionEnvironment .getExecutionEnvironment();

        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.setBufferTimeout(1);

        /**
         * Zookeeper Server Configuration
         */

        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "localhost:9092");
        properties.setProperty("zookeeper.connect", "localhost:2181");
        properties.setProperty("group.id", "" + "queryThree");

        /**
         * Kafka Topic Consumer
         */

        FlinkKafkaConsumer09<String> kafkaSource = new FlinkKafkaConsumer09<>(
                KafkaTopic, new SimpleStringSchema(), properties);

        kafkaSource.assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<String>(
                Time.milliseconds(100)) {
            @Override
            public long extractTimestamp(String s)
            {
                String[] event = s.split(",");

                return Long.parseLong(event[2]);
            }
        });


        /**
         * Preprocessing raw data
         */
        DataStream<Tuple6<String, Integer, Long, Integer, Integer, Double>> rawData =

                env   .addSource(kafkaSource)
                        .setParallelism(1)
                        .flatMap( new getSidPosSpeed());


        DataStream<Tuple4<String, Integer, Integer, Integer>> rawDataWithoutTimestamp = rawData.flatMap(new deleteTimestamp());


        /**
         *  TimeWindow: 1 min
         */
        DataStream<Tuple4< Long, String, Integer, Double[]>> statsPlayerW10_singleLeg = rawDataWithoutTimestamp
                .keyBy(0,1)
                .timeWindow(Time.minutes(1))
                .apply( new calcStatPlayerSingleLeg());


        DataStream<Tuple3<Long, String, Double[]>> statsPlayerW10 = statsPlayerW10_singleLeg .keyBy(0,1)
                .countWindow(2)
                .reduce( new calcStatPlayer())
                .flatMap( new deleteSidInfo());


        /**
         *  TimeWindow: 30 min
         */
        DataStream<Tuple3<Long, String, Double[]>> statsPlayerWD = statsPlayerW10
                                                                            .keyBy(1)
                                                                            .countWindow(30)
                                                                            .apply( new calcTimeWindow());



        statsPlayerW10  .writeAsText( PATH_OUTPUT_DIRECTORY+"/queryThree/timeWindow1.txt")
                        .setParallelism(1);

        statsPlayerWD   .writeAsText( PATH_OUTPUT_DIRECTORY+"/queryThree/timeWindow30.txt")
                        .setParallelism(1);

        env.execute("Query Three");
    }


    /**
     * PreProcessing Raw Data
     */
    public static class getSidPosSpeed implements FlatMapFunction<  String,
            Tuple6<String, Integer, Long, Integer, Integer, Double>>
    {

        @Override
        public void flatMap(String s,
                            Collector<Tuple6<String, Integer, Long, Integer, Integer, Double>> out) throws Exception
        {
            String[] event = s.split(",");

            String playername = event[0];

            int     sid = Integer.parseInt(event[1]),
                    x = Integer.parseInt(event[3]),
                    y = Integer.parseInt(event[4]);

            long timestamp = Long.parseLong(event[2]);

            double absSpeed = Double.parseDouble(event[5]);

            out.collect( new Tuple6<String, Integer, Long, Integer, Integer, Double>
                    ( playername, sid, timestamp, x, y, absSpeed));
        }
    }

    /**
     * QueryThree
     */
    public static class deleteTimestamp implements FlatMapFunction<
            Tuple6<String, Integer, Long, Integer, Integer, Double>,
            Tuple4<String, Integer, Integer, Integer>>
    {
        @Override
        public void flatMap(Tuple6<String, Integer, Long, Integer, Integer, Double> t1,
                            Collector<Tuple4<String, Integer, Integer, Integer>> out) throws Exception
        {
            out.collect( new Tuple4<String, Integer, Integer, Integer>
                    (t1.f0, t1.f1, t1.f3, t1.f4));
        }
    }

    public static class calcStatPlayerSingleLeg implements WindowFunction<
                Tuple4< String, Integer, Integer, Integer>,
                Tuple4< Long, String, Integer, Double[]>,
                Tuple, TimeWindow>
    {
        private int limSupY = 67930, limSupX = 52489;

        private Double[] fieldSingleLeg = new Double[104];

        private void initField() {
            int j = 0;

            for (; j < 104; ++j)
                fieldSingleLeg[j] = 0.0;
        }

        private int adaptYPos(int oldY) {
            int limSup = 33965;

            if (oldY >= 0) return Math.abs(oldY - limSup);
            else return Math.abs(oldY) + limSup;
        }

        private void calcPercTime(int i) {
            int j = 0;

            for (; j < 104; ++j)
                fieldSingleLeg[j] = fieldSingleLeg[j] / i * 100;
        }

        @Override
        public void apply(Tuple key, TimeWindow timeWindow,
                          Iterable<Tuple4< String, Integer, Integer, Integer>> t,
                          Collector<Tuple4<Long, String, Integer, Double[]>> out) throws Exception {
            initField();

            String player = "";
            int sid = 0, i = 0;


            for (Tuple4< String, Integer, Integer, Integer> tmp : t) {
                player = tmp.f0;
                sid = tmp.f1;

                int y = adaptYPos(tmp.f3),
                        posField = Math.round(y * 8 / limSupY) + Math.round(tmp.f2 * 13 / limSupX);

                fieldSingleLeg[posField]++;
                ++i;
            }

            calcPercTime(i);

            out.collect(new Tuple4<Long, String, Integer, Double[]>(timeWindow.getStart(), player, sid, fieldSingleLeg));
        }

    }

    public static class calcStatPlayer implements ReduceFunction<Tuple4<Long, String, Integer, Double[]>>
    {
        private Double[] field = new Double[104];

        private void initField() {
            int j = 0;

            for (; j < 104; ++j)
                field[j] = 0.0;
        }

        private void calcAvgStats(Tuple4<Long, String, Integer, Double[]> t1, Tuple4<Long, String, Integer, Double[]> t2) {
            int j = 0;

            for (; j < 104; ++j) {
                field[j] = (t1.f3[j] + t2.f3[j]) / 2;
            }
        }


        @Override
        public Tuple4<Long, String, Integer, Double[]> reduce(Tuple4<Long, String, Integer, Double[]> t1, Tuple4<Long, String, Integer, Double[]> t2) throws Exception {

            initField();

            calcAvgStats(t1, t2);

            return (new Tuple4<Long, String, Integer, Double[]>(t1.f0, t1.f1, t1.f2, field));

        }
    }

    public static class deleteSidInfo implements FlatMapFunction<Tuple4<Long, String, Integer, Double[]>,
            Tuple3<Long, String, Double[]>> {
        public void flatMap(Tuple4<Long, String, Integer, Double[]> t1,
                            Collector<Tuple3<Long, String, Double[]>> out) {
            out.collect(new Tuple3<Long, String, Double[]>(t1.f0, t1.f1, t1.f3));
        }
    }

    public static class calcTimeWindow implements WindowFunction <Tuple3<Long, String, Double[]>,
            Tuple3<Long, String, Double[]>,
            Tuple, GlobalWindow>
    {

        @Override
        public void apply(Tuple tuple, GlobalWindow timeWindow,
                          Iterable<Tuple3<Long, String, Double[]>> t,
                          Collector<Tuple3<Long, String, Double[]>> out) throws Exception
        {
            Double[] tmpPos = new Double[104];

            int i = 0,
                j = 0;

            while( j < 104 )
            {
                tmpPos[j] = 0.0;
                ++j;
            }

            long maxTimestamp = 0L;

            String player = "";



            for(Tuple3<Long, String, Double[]> tmp: t)
            {
                maxTimestamp = tmp.f0;


                j = 0;
                while( j < 104 )
                {
                    tmpPos[j] += tmp.f2[j];
                    ++j;
                }

                player = tmp.f1;

                ++i;
            }

            j = 0;
            while( j < 104 )
            {
                tmpPos[j] = tmpPos[j] / i;
                ++j;
            }

            out.collect( new Tuple3<Long, String, Double[]>
                    (       maxTimestamp,
                            player,
                            tmpPos
                            ));
        }
    }

}
