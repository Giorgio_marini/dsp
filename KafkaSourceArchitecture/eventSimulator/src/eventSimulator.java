import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 * @authors Giorgio Marini, Davide Nanni
 * @since 10/07/2017
 */
public class eventSimulator
{
    private static final String KAFKA_TOPIC = "playerEvents";

    private static KafkaProducer<String, String> producer;

    private static String path_dataset = "";

    private static long previousTimestamp = 0L;


    /**
     * Read the path raw sensors dataset
     * @param args
     */
    private static void readTestParameters(String[] args)
    {
        if ( args.length != 1)
        {
            System.out.println("[ERROR] Invalid Input! Required the <path_dataset> argument");
            System.exit(1);
        }

        path_dataset = args[0];
    }

    private static int sendEvents() throws IOException, InterruptedException {

        int numEvents = 0;
        String event = "";

        BufferedReader buff = new BufferedReader( new FileReader(path_dataset) );

        while( (event = buff.readLine()) != null)
        {
            producer.send(new ProducerRecord<String, String>( KAFKA_TOPIC, KAFKA_TOPIC, event));
            ++numEvents;
        }

        return numEvents;
    }

    private static void initKafkaProducer()
    {
        // create instance for properties to access producer configs
        Properties props = new Properties();

        //Assign localhost id
        props.put("bootstrap.servers", "localhost:9092");

        //Set acknowledgements for producer requests.
        props.put("acks", "all");

        //If the request fails, the producer can automatically retry,
        props.put("retries", 0);

        //Specify buffer size in config
        props.put("batch.size", 16384);

        //Reduce the no of requests less than 0
        props.put("linger.ms", 1);

        //The buffer.memory controls the total amount of memory available to the producer for buffering.
        props.put("buffer.memory", 33554432);

        props.put("key.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");

        props.put("value.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");

        producer = new KafkaProducer<String, String>(props);
    }

    public static void main(String args[]) throws IOException, InterruptedException {
        int numEvents = 0;

        readTestParameters(args);

        initKafkaProducer();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd HH:mm:ss");
        Date now = new Date();
        long start = now.getTime();

        System.out.println("[SENDING TRANSFORMED EVENTS]    "+sdf.format(start));

        numEvents = sendEvents();

        long stop = now.getTime();
        System.out.println("[ALL EVENTS ARE SENDED]         "+sdf.format( stop ));
        System.out.println("[TOT EVENTS] "+numEvents+" into "+(stop-start)+" ms");
    }
}
