import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.*;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.BoundedOutOfOrdernessTimestampExtractor;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer09;
import org.apache.flink.streaming.util.serialization.SimpleStringSchema;
import org.apache.flink.util.Collector;

import java.util.Iterator;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

/**
 * @authors Giorgio Marini, Davide Nanni
 * @since   11/07/2017
 *
 */
public class queryTwo
{
    private static String   PATH_OUTPUT_DIRECTORY = "";

    private static String KafkaTopic = "playerEvents";

    private static void readTestParameter(String[] args)
    {
        if (args.length != 1) {
            System.out.println("[ERROR] Invalid Input! Required <path-results> arguments");
            System.exit(1);
        }

        PATH_OUTPUT_DIRECTORY = args[0];
    }

    public static void main(String[] args) throws Exception
    {
        readTestParameter(args);

        StreamExecutionEnvironment env = StreamExecutionEnvironment .getExecutionEnvironment();

        env.setStreamTimeCharacteristic(TimeCharacteristic.EventTime);
        env.setBufferTimeout(1);

        /**
         * Zookeeper Server Configuration
         */

        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "localhost:9092");
        properties.setProperty("zookeeper.connect", "localhost:2181");
        properties.setProperty("group.id", "" + "queryTwo");

        /**
         * Kafka Topic Consumer
         */

        FlinkKafkaConsumer09<String> kafkaSource = new FlinkKafkaConsumer09<>(
                KafkaTopic, new SimpleStringSchema(), properties);

        kafkaSource.assignTimestampsAndWatermarks(new BoundedOutOfOrdernessTimestampExtractor<String>(
                Time.milliseconds(100)) {
            @Override
            public long extractTimestamp(String s)
            {
                String[] event = s.split(",");

                return Long.parseLong(event[2]);
            }
        });


        /**
         * Preprocessing raw data
         */
        DataStream<Tuple4<String, Integer, Long, Double>> rawData =

                env   .addSource(kafkaSource)
                        .setParallelism(1)
                        .flatMap( new getSidPosSpeed());


            DataStream<Tuple3<String, Integer, Double>> rawDataWithoutTimestamp = rawData   .flatMap(new deleteTimestamp());

            DataStream<Tuple5<Long, Long, String, Integer, Double>> avgSpeedSingleLeg_W1 =  rawDataWithoutTimestamp
                                                                                            .keyBy(0,1)
                                                                                            .timeWindow(Time.minutes(1))
                                                                                            .apply( new calcAvgSpeedSingleLeg());

            DataStream<Tuple4<Long, Long, String, Double>> avgSpeed_W1 = avgSpeedSingleLeg_W1   .keyBy(0,1,2)
                                                                                                .countWindow(2)
                                                                                                .apply( new calcAvgSpeed());

            DataStream<Tuple3<Long, Long, String>> rankingW1 = avgSpeed_W1  .keyBy(0,1)
                                                                            .countWindow(16)
                                                                            .apply( new calcRating());

        /**
         * TimeWindow: 5 min
         */

        DataStream<Tuple4<Long, Long, String, Double>> avgSpeedPlayerW5 = avgSpeed_W1.keyBy(2)
                                                                                       .countWindow(5)
                                                                                       .apply( new calcAvgSpeedPlayerTimeWindow());

        DataStream<Tuple3<Long, Long, String>> rankingW5 = avgSpeedPlayerW5  .keyBy(0,1)
                .countWindow(16)
                .apply( new calcRating());

        /**
         * TimeWindow: 30 min (allDataset)
         */

        DataStream<Tuple4<Long, Long, String, Double>> avgSpeedPlayerW30 = avgSpeedPlayerW5.keyBy(2)
                .countWindow(6)
                .apply( new calcAvgSpeedPlayerTimeWindow());

        DataStream<Tuple3<Long, Long, String>> rankingW30 = avgSpeedPlayerW30  .keyBy(0,1)
                .countWindow(16)
                .apply( new calcRating());


        rankingW1   .writeAsText(PATH_OUTPUT_DIRECTORY+"/queryTwo/timeWindow1.txt")
                    .setParallelism(1);

        rankingW5   .writeAsText(PATH_OUTPUT_DIRECTORY+"/queryTwo/timeWindow5.txt")
                    .setParallelism(1);

        rankingW30  .writeAsText(PATH_OUTPUT_DIRECTORY+"/queryTwo/timeWindow30.txt")
                    .setParallelism(1);

        env.execute("Query Two");
    }

    /**
     * PreProcessing Raw Data
     */
    public static class getSidPosSpeed implements FlatMapFunction<  String,
            Tuple4<String, Integer, Long, Double>>
    {

        @Override
        public void flatMap(String s,
                            Collector<Tuple4<String, Integer, Long, Double>> out) throws Exception
        {
            String[] event = s.split(",");

            String playername = event[0];

            int     sid = Integer.parseInt(event[1]);

            long timestamp = Long.parseLong(event[2]);

            double absSpeed = Double.parseDouble(event[5]);

            out.collect( new Tuple4<String, Integer, Long, Double>
                    ( playername, sid, timestamp, absSpeed));
        }
    }

    /**
     * QueryThree
     */
    public static class deleteTimestamp implements FlatMapFunction<
            Tuple4<String, Integer, Long, Double>,
            Tuple3<String, Integer, Double>>
    {
        @Override
        public void flatMap(Tuple4<String, Integer, Long, Double> t1,
                            Collector<Tuple3<String, Integer, Double>> out) throws Exception
        {
            out.collect( new Tuple3<String, Integer, Double>
                    (t1.f0, t1.f1, t1.f3));
        }
    }


    public static class calcAvgSpeedSingleLeg implements WindowFunction<Tuple3<String, Integer, Double>,
                Tuple5<Long, Long, String, Integer, Double>, Tuple, TimeWindow>
    {

        @Override
        public void apply(Tuple tuple, TimeWindow timeWindow,
                          Iterable<Tuple3<String, Integer, Double>> t,
                          Collector<Tuple5<Long, Long, String, Integer, Double>> out) throws Exception
        {
            double avgSpeed = 0.0;

            int i = 0,
                sid = 0;

            String player = "";

            for(Tuple3<String, Integer, Double> tmp: t)
            {
                player = tmp.f0;
                sid = tmp.f1;

                avgSpeed += tmp.f2;

                ++i;
            }

            avgSpeed = avgSpeed / i;

            out.collect( new Tuple5< Long, Long, String, Integer, Double>
                         (timeWindow.getStart(), timeWindow.getEnd(),
                           player, sid, avgSpeed ));
        }
    }


    public static class calcAvgSpeed implements WindowFunction< Tuple5<Long, Long, String, Integer, Double>,
                                                                Tuple4<Long, Long, String, Double>,
                                                                Tuple, GlobalWindow>
    {

        @Override
        public void apply(Tuple tuple,
                          GlobalWindow timeWindow,
                          Iterable<Tuple5<Long, Long, String, Integer, Double>> t,
                          Collector<Tuple4<Long, Long, String, Double>> out) throws Exception
        {
            double avgSpeed = 0.0;

            long    timestamp_start = 0L,
                    timestamp_end = 0L;

            String player = "";


            for( Tuple5<Long, Long, String, Integer, Double> tmp: t)
            {
                timestamp_start = tmp.f0;
                timestamp_end = tmp.f1;

                player = tmp.f2;

                avgSpeed += tmp.f4;
            }

            avgSpeed = avgSpeed / 2;

            out.collect( new Tuple4<Long, Long, String, Double>
                            (timestamp_start, timestamp_end, player, avgSpeed));
        }
    }


    static class RatingPlayer implements Comparable<RatingPlayer>
    {
        private String player = "";

        private double avgSpeed = 0.0;

        public RatingPlayer(String player,
                            double avgSpeed)
        {
            this.player = player;
            this.avgSpeed = avgSpeed;
        }

        public String getPlayer()
        {
            return player;
        }

        public double getAvgSpeed()
        {
            return avgSpeed;
        }

        @Override
        public int compareTo(RatingPlayer o)
        {
            if ( this.avgSpeed < o.avgSpeed )
                return 1;
            else
            {
                if ( this.avgSpeed == o.avgSpeed )
                    return 0;
                else
                    return -1;
            }
        }
    }

    private static class calcRating implements WindowFunction<  Tuple4<Long, Long, String, Double>,
            Tuple3<Long, Long, String>,
            Tuple, GlobalWindow>
    {
        @Override
        public void apply(Tuple tuple, GlobalWindow globalWindow,
                          Iterable<Tuple4<Long, Long, String, Double>> t1,
                          Collector<Tuple3<Long, Long, String>> t2) throws Exception
        {
            Set<RatingPlayer> p = new TreeSet<RatingPlayer>();

            long    timestamp_start = 0L,
                    timestamp_stop = 0L;

            for( Tuple4<Long, Long, String, Double> tmp: t1)
            {
                p.add( new RatingPlayer( tmp.f2, tmp.f3 ));

                timestamp_start = tmp.f0;
                timestamp_stop = tmp.f1;
            }

            Iterator<RatingPlayer> i = p.iterator();

            String finalRating =    "";

            int j = 0;

            while( ( i.hasNext()) && (j < 5))
            {
                RatingPlayer tmp = i.next();

                finalRating +=  tmp.getPlayer()+","+
                        tmp.getAvgSpeed()+",";

                ++j;
            }

            finalRating = finalRating.substring(0,finalRating.length()-1);

            t2.collect( new Tuple3<Long, Long, String>
                    ( timestamp_start, timestamp_stop, finalRating));
        }
    }


    private static class calcAvgSpeedPlayerTimeWindow implements WindowFunction<  Tuple4<Long, Long, String, Double>,
            Tuple4<Long, Long, String, Double>,
            Tuple, GlobalWindow>
    {

        @Override
        public void apply(Tuple tuple, GlobalWindow globalWindow,
                          Iterable<Tuple4<Long, Long, String, Double>> t,
                          Collector<Tuple4<Long, Long, String, Double>> out) throws Exception
        {
            double avgSpeed = 0.0;

            long    timestamp_start = 0L,
                    timestamp_stop = 0L;

            String player = "";

            int i = 0;

            for(Tuple4<Long, Long, String, Double> tmp: t)
            {
                if ( i == 0 )
                    timestamp_start = tmp.f0;

                timestamp_stop = tmp.f1;
                player = tmp.f2;
                avgSpeed += tmp.f3;

                ++i;
            }

            avgSpeed = avgSpeed / i;

            out.collect( new Tuple4<Long, Long, String, Double>
                            ( timestamp_start, timestamp_stop,
                                    player,
                                    avgSpeed));

        }
    }
}
